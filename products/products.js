"use strict";

(function (exports) {
    var currDir; // current product type differentiated by the company name {gm, fil3, flp, trendfil, others}
    var listType;  // {- 0 means with modal list; 1 means without modal list}

    var collections = []; // collection address with the images dir prefix
    var collectionNames = []; // array of collection names
    var collectionColorMappings = {}; // mapping collectionName with its colors array

    var collectionDirNameMappings = {}; // mapping collection dir with its name
    var collectionDirs = []; // array of collection dirs


    /**
     * Update list in one page
     * @param colID
     * @param index
     */
    var updateLists = function(colID, index) {
        var col1Access = colID + " #col1img";
        var col2Access = colID + " #col2img";
        var col3Access = colID + " #col3img";

        var col1 = colID + " #col1";
        var col2 = colID + " #col2";
        var col3 = colID + " #col3";

        var col1PAccess = colID + " #col1P";
        var col2PAccess = colID + " #col2P";
        var col3PAccess = colID + " #col3P";

        if (listType === 0) {


            if (index > collectionNames.length - 1) {
                $(col1).html("");
                $(col2).html("");
                $(col3).html("");
                return;
            }

            $(col1Access).attr("src", collections[index]);
            $(col1Access).attr("alt", collectionNames[index]);
            $(col1PAccess).text(collectionNames[index]);

            if (index + 1 > collectionNames.length - 1) {
                $(col2).html("");
                $(col3).html("");
                return;
            }

            $(col2Access).attr("src", collections[index + 1]);
            $(col2Access).attr("alt", collectionNames[index + 1]);
            $(col2PAccess).text(collectionNames[index + 1]);


            if (index + 2 > collectionNames.length - 1) {
                $(col3).html("");
                return;
            }


            $(col3Access).attr("src", collections[index + 2]);
            $(col3Access).attr("alt", collectionNames[index + 2]);
            $(col3PAccess).text(collectionNames[index + 2]);
        } else if (listType === 1) {
            if (index > collectionDirs.length-1) {
                $(col1).html("");
                $(col2).html("");
                $(col3).html("");
                return;
            }

            var collect1 = collectionDirs[index];
            $(col1Access).attr("src", collect1);
            $(col1Access).attr("alt",collectionDirNameMappings[collect1]);

            $(col1PAccess).text(collectionDirNameMappings[collect1]);


            if (index+1 > collectionDirs.length-1) {
                $(col2).html("");
                $(col3).html("");
                return;
            }

            var collect2 = collectionDirs[index+1];
            $(col2Access).attr("src", collect2);
            $(col2Access).attr("alt",collectionDirNameMappings[collect2]);
            $(col2PAccess).text(collectionDirNameMappings[collect2]);

            if (index+2 > collectionDirs.length-1) {
                $(col3).html("");
                return;
            }

            var collect3 = collectionDirs[index+2];
            $(col3Access).attr("src", collect3);
            $(col3Access).attr("alt",collectionDirNameMappings[collect3]);
            $(col3PAccess).text(collectionDirNameMappings[collect3]);
        }
    };


    /**
     * Upload images
     */
    var uploadImages = function () {
        var e = $('#row');
        var rowPrefix = 'row';
        var colNumToClone = 0;
        if (currDir === 'gm' || currDir === 'trendfil' || currDir === 'flp') {
            colNumToClone = 3;
        } else if (currDir === 'fil3') {
            colNumToClone = 2;
        } else if (currDir === 'others_w/o_sample') {
            colNumToClone = 3;
        } else if (currDir === 'others_ava') {
            colNumToClone = 20;
        } else if (currDir === 'others') {
            colNumToClone = 6;
        }
        else{
            console.assert('Invalid company name');
        }

        var prev = e;
        for (var i = 0; i <= colNumToClone; i++) {
            var cloned = e.clone();
            var j = i + 1;

            // Clone the gm row and change its id
            var clonedID = rowPrefix + j;
            cloned.attr('id', clonedID);
            cloned.insertAfter(prev);

            updateLists('#' + clonedID, j*3);
            prev = $('#' + clonedID);
        }


    };


    var currentSlideIndex = 1;

    /**
     * Open the color modal
     * @param name - sample name
     */
    exports.openModal = function (name) {
        currentSlideIndex = 1;
        $('#slides').html('');
        $('#caption').text('');
        var updated = updateSlides(name);
        if (updated >= 1) {
            showSlides(currentSlideIndex);
            if (updated > 1) {
                $('.prev').show();
                $('.next').show();
            } else {
                $('.prev').hide();
                $('.next').hide();
            }
        }
       // $('body').css('overflow','hidden');
        $('#colorModal').modal('show');
        window.location.hash = '#modal';
    };

    $(window).on('hashchange', function (event) {
        if(window.location.hash !== "#modal") {
            $('#colorModal').modal('hide');
        }
        if(window.location.hash === "#modal") {
            $('#colorModal').modal('show');
        }
    });

    /**
     * Remove the '#modal' hash when closing the modal
     */
    exports.removeHash = function () {
        if(window.location.hash === "#modal") {
            history.pushState(null, null, window.location.href.split('#')[0]);
        }
    };

    /**
     * update colors of the sample
     * @param name - sample name
     * @returns {boolean}
     */
    var updateSlides = function (name) {
        $('.modal-title').text('色卡 | '+ name);
        var colors = collectionColorMappings[name];
        var colorNum = colors.length;
        var numberClass = 'numbertext';

        if (colorNum === 0) {
            $('.prev').hide();
            $('.next').hide();
            $("#caption").text("此样品没有色卡");
        } else {
            if (name === 'AIDA (50SM10W40PA)' || name === 'GILDA (50SM10W40PA)' ||
                name === 'DUDA COTTON (45WP20W35CO)' || name === 'FAUST (38W18CO44PA)' ||
                name === 'FENICE (54CO35MD11V)' || name === 'SF JAZZ (8WP8W70CO14AC)' ||
                name === 'NICHT (55WP20W25PA)' || name === 'SUITE (70WP30PA)' || name === 'SYNTH HARP (10WP10W60PL20AC)'||
                name === 'Market (47PA45WO8WP)' || name === 'Mattina (50AC34PA10WM6WO)' ||
                name === 'Marzia Più (40AC30WM30PA)' || name === 'Twill (36PA28AC23PES13WO)' ||
                name === 'Matri (40PA40AC20WM)' || name === 'Lumia (63CV20MEPES17PA)' ||
                name === 'Park (60WO30PA10WP)' || name === 'Sportivo (65AC22WM10WO3PA)' ||
                name === 'Duvet (35AC31PES17WO17PA)' || name === 'Grisaille (100CO)' ||
                name === 'Marzia (40AC30WM30PA)') {
                numberClass = 'numbertextW';
            }
            var colorDirPrefix = '../images/' + currDir + '/colors/';
            if (currDir === 'others_ava') {
                var colorDirPrefix = '../images/others/avaColors/';
            }
            for (var i = 0; i < colorNum; i++) {
                var j = i+1;

                var color = colorDirPrefix + colors[i];
                var colorClass = 'mySlides';
                if (colors[i] === 'GOLDEN_FIAMMINO_SILVER_PICASSO_NATRON_FIAMMONE_1.JPG') {
                    colorClass = colorClass + ' long';
                }

                var insertText = '<div class="'+ colorClass + '"><div class="'+ numberClass +'">'+ j + ' / ' + colorNum +
                    '</div><img src="'+ color +  '"></div>';
                $('#slides').append(insertText);
            }
        }
        return colorNum;
    };


    /**
     * Show slide
     * @param slideIdx
     */
    function showSlides(slideIdx) {
        var temp = slideIdx;
        var slides = document.getElementsByClassName('mySlides');
        if (temp > slides.length) {
            currentSlideIndex = 1;
        }
        if (temp < 1) {
            currentSlideIndex = slides.length;
        }

        for (var i = 0; i < slides.length; i++) {
            slides[i].style.display = 'none';
        }
        if ($(slides[currentSlideIndex-1]).hasClass('long') || currDir === 'others_ava') {
            if ($('.prev').not('#avaPrev')) {
                $('.prev').attr('id', 'avaPrev');
            }
            if ($('.next').not('#avaNext')) {
                $('.next').attr('id', 'avaNext');
            }
        } else {
            if ($('.prev').is('#avaPrev')) {
                $('.prev').removeAttr('id');
            }
            if ($('.next').is('#avaNext')) {
                $('.next').removeAttr('id');
            }
        }

        slides[currentSlideIndex-1].style.display = 'block';
        $('.caption').text('');
    }


    /**
     * Execution function for 'prev' & 'next' button in the modal
     * @param n {-1 means previous page; +1 means next page}
     */
    exports.plusSlides = function(n) {
        showSlides(currentSlideIndex += n);
    };


    /**
     * Load json data from package.json file
     * @param dir
     */
    exports.loadData = function (dir) {
        if (dir === 'gm1' || dir === 'gm2'
            || dir === 'gm3') {
            currDir = 'gm';
        } else if (dir === 'trendfil1'
            || dir === 'trendfil2' || dir === 'trendfil3') {
            currDir = 'trendfil';
        } else {
            currDir = dir;
        }

        var getData = $.getJSON('../package.json');
        getData.done(function (data) {
            if (data !== {}) {
                if (dir === 'gm1' || dir === 'gm2'
                    || dir === 'gm3' || dir === 'fil3' || dir === 'others' || dir === 'others_ava') {
                    listType = 0;
                    var json = data[dir];
                    $.each(json, function (key, val) {
                        var collectionAddrPrefix;
                        var collectionName;
                        if (dir === 'others_ava') {
                            collectionAddrPrefix = '../images/others/avaCollections/';
                            var tempKey = key.slice(0);
                            collectionName = tempKey.replace(/:/, '/');
                            collectionName = collectionName.substr(0, key.indexOf('.')); // get the name without .jpg ext
                        } else {
                            collectionAddrPrefix = '../images/' + currDir + '/collections/';
                            collectionName = key.substr(0, key.indexOf('.')); // get the name without .jpg ext
                        }
                        collectionNames.push(collectionName);
                        var key = collectionAddrPrefix + key;

                        collectionColorMappings[collectionName] = val;
                        collections.push(key);
                    });
                } else if (dir === 'flp' || dir === 'trendfil1'
                    || dir === 'trendfil2' || dir === 'trendfil3'
                    || dir === 'others_w/o_sample') {
                    listType = 1;
                    collectionDirNameMappings = data[dir];
                    $.each(collectionDirNameMappings, function (key, val) {
                        collectionDirs.push(key);
                    });
                } else {
                    console.assert('Invalid company name.');
                }
                uploadImages();
            }});
        };

    /* Click & Unclick */
    var prevClicked = '';
    var prevID = '';

    exports.clickEffect = function (img) {
        var accessID = '#' + img.parentNode.parentNode.id + ' #' + img.id;
       // var accessID = '#'+ imgId;
        var alt = $(accessID).attr('alt');
        if (alt !== prevClicked) {
            $(prevID).css('border','none');
        }

        $(accessID).css('border','5px solid #4286f4');

        prevID = accessID;
        prevClicked = alt;
    };


    $(document).click(function(e) {
        if((!$(e.target).is(prevID)) && !$('#colorModal').is(':visible')) {
            $(prevID).css('border','none');
        }
    });



    // Credit to w3schools bootstrap template
    $(document).ready(function(){
        // Add smooth scrolling to all links in navbar + footer link
        $("#goToTop a, footer a[href='#top']").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 1500, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });

        $(window).scroll(function() {
            $(".slideanim").each(function(){
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    });

})(window);
